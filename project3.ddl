-- Generated by Oracle SQL Developer Data Modeler 4.1.3.901
--   at:        2016-10-22 13:19:26 CDT
--   site:      Oracle Database 11g
--   type:      Oracle Database 11g




DROP TABLE color CASCADE CONSTRAINTS ;

DROP TABLE color_species CASCADE CONSTRAINTS ;

DROP TABLE family CASCADE CONSTRAINTS ;

DROP TABLE genus CASCADE CONSTRAINTS ;

DROP TABLE gss CASCADE CONSTRAINTS ;

DROP TABLE sighting CASCADE CONSTRAINTS ;

DROP TABLE speciesv1 CASCADE CONSTRAINTS ;

CREATE TABLE color
  ( color_id INTEGER NOT NULL , color_name VARCHAR2 (256)
  ) ;
ALTER TABLE color ADD CONSTRAINT sub_clan_PK PRIMARY KEY ( color_id ) ;


CREATE TABLE color_species
  (
    color_species_id INTEGER NOT NULL ,
    color_id         INTEGER NOT NULL ,
    species_id       INTEGER NOT NULL
  ) ;
ALTER TABLE color_species ADD CONSTRAINT mem_sub_PK PRIMARY KEY ( color_species_id ) ;


CREATE TABLE family
  (
    family_id   INTEGER NOT NULL ,
    family_name VARCHAR2 (256)
  ) ;
ALTER TABLE family ADD CONSTRAINT division_PK PRIMARY KEY ( family_id ) ;


CREATE TABLE genus
  (
    genus_id   INTEGER NOT NULL ,
    genus_name VARCHAR2 (256) ,
    family_id  INTEGER ,
    species_id INTEGER
  ) ;
CREATE UNIQUE INDEX genus__IDX ON genus
  (
    species_id ASC
  )
  ;
ALTER TABLE genus ADD CONSTRAINT main_clan_PK PRIMARY KEY ( genus_id ) ;


CREATE TABLE gss
  (
    gss_id      INTEGER NOT NULL ,
    genus_id    INTEGER NOT NULL ,
    sighting_id INTEGER NOT NULL ,
    species_id  INTEGER NOT NULL
  ) ;
ALTER TABLE gss ADD CONSTRAINT cmm_PK PRIMARY KEY ( gss_id ) ;


CREATE TABLE sighting
  (
    sighting_id INTEGER NOT NULL ,
    "date"      DATE ,
    location    VARCHAR2 (256)
  ) ;
ALTER TABLE sighting ADD CONSTRAINT match_PK PRIMARY KEY ( sighting_id ) ;


CREATE TABLE speciesv1
  (
    species_id     INTEGER NOT NULL ,
    species_name   VARCHAR2 (256) ,
    characteristic VARCHAR2 (256) ,
    plant_id       INTEGER ,
    animal_id      INTEGER ,
    type           VARCHAR2 (9) NOT NULL
  ) ;
ALTER TABLE speciesv1 ADD CONSTRAINT CH_INH_speciesv1 CHECK ( type IN ('animalia', 'plantae', 'speciesv1')) ;
ALTER TABLE speciesv1 ADD CONSTRAINT speciesv1_ExDep CHECK ( (type = 'animalia' AND plant_id IS NULL AND animal_id IS NOT NULL) OR (type = 'plantae' AND plant_id IS NOT NULL AND animal_id IS NULL) OR (type = 'speciesv1' AND plant_id IS NULL AND animal_id IS NULL)) ;
CREATE UNIQUE INDEX speciesv1__IDX ON speciesv1 ( species_id ASC ) ;
ALTER TABLE speciesv1 ADD CONSTRAINT speciesv1_PK PRIMARY KEY ( species_id ) ;
ALTER TABLE speciesv1 ADD CONSTRAINT animalia_PK UNIQUE ( animal_id ) ;
ALTER TABLE speciesv1 ADD CONSTRAINT plantae_PK UNIQUE ( plant_id ) ;


ALTER TABLE gss ADD CONSTRAINT Relation_7 FOREIGN KEY ( sighting_id ) REFERENCES sighting ( sighting_id ) ;

ALTER TABLE color_species ADD CONSTRAINT r11v1 FOREIGN KEY ( species_id ) REFERENCES speciesv1 ( species_id ) ;

ALTER TABLE genus ADD CONSTRAINT r12 FOREIGN KEY ( species_id ) REFERENCES speciesv1 ( species_id ) ;

ALTER TABLE gss ADD CONSTRAINT r13 FOREIGN KEY ( species_id ) REFERENCES speciesv1 ( species_id ) ;

ALTER TABLE color_species ADD CONSTRAINT r2 FOREIGN KEY ( color_id ) REFERENCES color ( color_id ) ;

ALTER TABLE gss ADD CONSTRAINT r5 FOREIGN KEY ( genus_id ) REFERENCES genus ( genus_id ) ;

ALTER TABLE genus ADD CONSTRAINT r6 FOREIGN KEY ( family_id ) REFERENCES family ( family_id ) ;

CREATE SEQUENCE color_color_id_SEQ START WITH 1 NOCACHE ORDER ;
CREATE OR REPLACE TRIGGER color_color_id_TRG BEFORE
  INSERT ON color FOR EACH ROW WHEN (NEW.color_id IS NULL) BEGIN :NEW.color_id := color_color_id_SEQ.NEXTVAL;
END;
/

CREATE SEQUENCE color_species_color_species_id START WITH 1 NOCACHE ORDER ;
CREATE OR REPLACE TRIGGER color_species_color_species_id BEFORE
  INSERT ON color_species FOR EACH ROW WHEN (NEW.color_species_id IS NULL) BEGIN :NEW.color_species_id := color_species_color_species_id.NEXTVAL;
END;
/

CREATE SEQUENCE family_family_id_SEQ START WITH 1 NOCACHE ORDER ;
CREATE OR REPLACE TRIGGER family_family_id_TRG BEFORE
  INSERT ON family FOR EACH ROW WHEN (NEW.family_id IS NULL) BEGIN :NEW.family_id := family_family_id_SEQ.NEXTVAL;
END;
/

CREATE SEQUENCE genus_genus_id_SEQ START WITH 1 NOCACHE ORDER ;
CREATE OR REPLACE TRIGGER genus_genus_id_TRG BEFORE
  INSERT ON genus FOR EACH ROW WHEN (NEW.genus_id IS NULL) BEGIN :NEW.genus_id := genus_genus_id_SEQ.NEXTVAL;
END;
/

CREATE SEQUENCE gss_gss_id_SEQ START WITH 1 NOCACHE ORDER ;
CREATE OR REPLACE TRIGGER gss_gss_id_TRG BEFORE
  INSERT ON gss FOR EACH ROW WHEN (NEW.gss_id IS NULL) BEGIN :NEW.gss_id := gss_gss_id_SEQ.NEXTVAL;
END;
/

CREATE SEQUENCE sighting_sighting_id_SEQ START WITH 1 NOCACHE ORDER ;
CREATE OR REPLACE TRIGGER sighting_sighting_id_TRG BEFORE
  INSERT ON sighting FOR EACH ROW WHEN (NEW.sighting_id IS NULL) BEGIN :NEW.sighting_id := sighting_sighting_id_SEQ.NEXTVAL;
END;
/

CREATE SEQUENCE speciesv1_species_id_SEQ START WITH 1 NOCACHE ORDER ;
CREATE OR REPLACE TRIGGER speciesv1_species_id_TRG BEFORE
  INSERT ON speciesv1 FOR EACH ROW WHEN (NEW.species_id IS NULL) BEGIN :NEW.species_id := speciesv1_species_id_SEQ.NEXTVAL;
END;
/


-- Oracle SQL Developer Data Modeler Summary Report: 
-- 
-- CREATE TABLE                             7
-- CREATE INDEX                             2
-- ALTER TABLE                             18
-- CREATE VIEW                              0
-- ALTER VIEW                               0
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           7
-- ALTER TRIGGER                            0
-- CREATE COLLECTION TYPE                   0
-- CREATE STRUCTURED TYPE                   0
-- CREATE STRUCTURED TYPE BODY              0
-- CREATE CLUSTER                           0
-- CREATE CONTEXT                           0
-- CREATE DATABASE                          0
-- CREATE DIMENSION                         0
-- CREATE DIRECTORY                         0
-- CREATE DISK GROUP                        0
-- CREATE ROLE                              0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE SEQUENCE                          7
-- CREATE MATERIALIZED VIEW                 0
-- CREATE SYNONYM                           0
-- CREATE TABLESPACE                        0
-- CREATE USER                              0
-- 
-- DROP TABLESPACE                          0
-- DROP DATABASE                            0
-- 
-- REDACTION POLICY                         0
-- 
-- ORDS DROP SCHEMA                         0
-- ORDS ENABLE SCHEMA                       0
-- ORDS ENABLE OBJECT                       0
-- 
-- ERRORS                                   0
-- WARNINGS                                 0
